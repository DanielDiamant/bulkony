# Bulkony

A bulks-generator tool

## exmplae
```javascript
const { BulksGenerator } = require("bulkony");

const bulker = new BulksGenerator({bulkSize: 3});

bulker.onBulk((bulk) => console.log(bulk));

for(let i=1; i<=10; i++)
    bulker.push(i);
bulker.flush();
```
will output:
```javascript
[1, 2, 3]
[4, 5, 6]
[7, 8, 9]
[10]
```

## API

### BulksGenerator

#### Constructor

```javascript
const bulker = new BulksGenerator(options, onBulk);
```
##### options
| Option Name | Type | Description |
|-|-|-|-|
| bulkSize | number | The number of elements in the bulkwill cause the bulker to dispatch|

##### onBulk
A callback thet will be called on each bulk.
For example:
```javascript
const onBulk = (bulk) => console.log(bulk);
```
 

#### .onBulk(callback)
Set the OnBulk with new callback.

#### .push(item)
Process a new item

#### .flush()
Pack the left items and send as a bulk, even if it has'nt reached `bulkSize`.

#### .iterate(generator)
Gets an iterable prop and handles it's iteration through bulker.
```javascript
const bulker = new BulksGenerator({bulkSize: 3});
bulker.onBulk(console.log);
bulker.iterate([1,2,3,4,5]);
```
will output:
```javascript
[1, 2, 3]
[4, 5]
```

### AsyncBulksGenerator
same as `BulksGenerator` but supports `async`/`await` mechanism

#### async onBulk
```javascript
async function onBulk(bulk) {
    console.log(bulk);
    await new Promise(r => setTimeout(r, 500));
}
const bulksGenerator = new AsyncBulksGenerator({ bulkSize: 3 }, onBulk);
await bulksGenerator.iterate([1,2,3,4,5]);
console.log("complete");
```

will outout:
```javascript
[1, 2, 3]
// waits 500ms
[4, 5]
// waits 500ms
complete
```

#### async `.push()` and `.flush()`

```javascript
async function onBulk(bulk) {
    console.log(bulk);
    await new Promise(r => setTimeout(r, 500));
}
const bulksGenerator = new AsyncBulksGenerator({ bulkSize: 3 }, onBulk);
for(let i of [1,2,3,4])
    await bulksGenerator.push(i);
await bulksGenerator.flush();
console.log("complete");
```

will output:
```javascript
[1, 2, 3]
// waits 500ms
[4, 5]
// waits 500ms
complete
```

## Usage with generators
```javascript
const range = function*(from, to){
    for(let i=from; i<=to; i++)
        yield i;
}

const bulker = new BulksGenerator({bulkSize: 3});
bulker.onBulk(console.log);
bulker.iterate(range(10,20));
```
will output:
```javascript
[ 10, 11, 12 ]
[ 13, 14, 15 ]
[ 16, 17, 18 ]
[ 19, 20 ]
```
\* `AsyncBulksGenerator` does supports `AsyncGenerator` objects