const BulksGenerator = require("./bulks-generator");

class AsyncBulksGenerator extends BulksGenerator {
    async _dispatch() {
        await this._onBulk(this._bulk);
        this._bulk = [];
    }

    async iterate(generator) {
        for await (let item of generator)
            await this.push(item);
        await this.flush();
    }
}

module.exports = AsyncBulksGenerator;