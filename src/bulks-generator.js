class BulksGenerator {
    constructor({ bulkSize }, onBulk) {
        this._bulkSize = bulkSize;
        this._onBulk = onBulk;
        this._bulk = [];
    }

    _dispatch() {
        this._onBulk(this._bulk);
        this._bulk = [];
    }

    onBulk(onBulk){
        this._onBulk = onBulk;
    }

    push(item) {
        this._bulk.push(item);
        if (this._bulk.length >= this._bulkSize)
            return this._dispatch();
    }

    flush() {
        if (this._bulk.length > 0)
            return this._dispatch();
    }

    iterate(generator) {
        for (let item of generator)
            this.push(item);
        this.flush();
    }
}

module.exports = BulksGenerator;