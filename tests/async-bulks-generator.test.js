const { AsyncBulksGenerator } = require("../");
const { assert } = require("chai");
const sinon = require("sinon");

describe("Async BulksGenerator", function () {
    it("Supports push", async function () {
        let finished = false;
        const onBulk = async () => {
            await new Promise(r => setTimeout(r, 0));
            finished = true;
        }
        const bulksGenerator = new AsyncBulksGenerator({ bulkSize: 1 }, onBulk);
        await bulksGenerator.push(1);
        assert.equal(finished, true);
    })

    it("Supports .iterate()", async function () {
        const input = [1, 2, 3, 4, 5];
        const exporterOutput = [[1, 2, 3], [4, 5]];
        const outputBulks = [];
        const onBulk = (item) => outputBulks.push(item);
        const bulksGenerator = new AsyncBulksGenerator({ bulkSize: 3 }, onBulk);
        await bulksGenerator.iterate(input);
        assert.deepEqual(outputBulks, exporterOutput);
    })
})