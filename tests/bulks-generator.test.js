const { BulksGenerator } = require("../");
const { assert } = require("chai");
const sinon = require("sinon");

describe("BulksGenerator", function () {
    it("Generate bulks", function () {
        const onBulk = sinon.spy();
        const bulksGenerator = new BulksGenerator({ bulkSize: 3 }, onBulk);

        for (let i = 0; i < 7; i++)
            bulksGenerator.push(i);
        bulksGenerator.flush();

        assert.deepEqual(onBulk.args.map(i => i[0]), [[0, 1, 2], [3, 4, 5], [6]]);
    });

    it("Not waiting for async onBulk", async function () {
        const spy = sinon.spy();
        const onBulk = async () => {
            await new Promise(r => setTimeout(r, 0));
            spy();
        }
        const bulksGenerator = new BulksGenerator({ bulkSize: 1 }, onBulk);
        await bulksGenerator.push(1);
        sinon.assert.notCalled(spy);
    })

    it("Supports .iterate()", function () {
        const input = [1, 2, 3, 4, 5];
        const exporterOutput = [[1, 2, 3], [4, 5]];
        const outputBulks = [];
        const onBulk = (item) => outputBulks.push(item);
        const bulksGenerator = new BulksGenerator({ bulkSize: 3 }, onBulk);
        bulksGenerator.iterate(input);
        assert.deepEqual(outputBulks, exporterOutput);
    })

    it("Supports .onBulk()", function () {
        const onBulk = sinon.spy();
        const bulksGenerator = new BulksGenerator({ bulkSize: 1 });
        bulksGenerator.onBulk(onBulk);
        bulksGenerator.push(1);
        sinon.assert.calledOnceWithExactly(onBulk, [1]);
    })
})